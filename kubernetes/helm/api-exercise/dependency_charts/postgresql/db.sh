#!/bin/bash

set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE TABLE public.people (
	uuid uuid NOT NULL,
	age int4 NOT NULL,
	fare float8 NOT NULL,
	"name" varchar(255) NULL,
	parents_or_children_aboard int4 NOT NULL,
	passenger_class int4 NOT NULL,
	sex varchar(255) NULL,
	siblings_or_spouses_aboard int4 NOT NULL,
	survived bool NOT NULL,
	CONSTRAINT people_pkey PRIMARY KEY (uuid)
    );
    COPY public.people(age,fare,name,parents_or_children_aboard,passenger_class,sex,siblings_or_spouses_aboard,survived)
    FROM 'C:\tmp\sample_data.csv' DELIMITER ',' CSV HEADER;
EOSQL