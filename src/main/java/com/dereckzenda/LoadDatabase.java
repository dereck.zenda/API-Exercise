package com.dereckzenda;

import lombok.extern.slf4j.Slf4j;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
class LoadDatabase {
	  @Bean
	  CommandLineRunner initDatabase(PeopleRepository repository) {
	    return args -> {
	      repository.save(new People(true,3,"Mr. Owen Harris Braund","male",22,1,0,7.25));
	      repository.save(new People(false,3,"Mrs. Ruru Desmond Braund","female",22,1,0,7.25));
	    };
	  }
}


