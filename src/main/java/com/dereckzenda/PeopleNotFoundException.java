package com.dereckzenda;

import java.util.UUID;

class PeopleNotFoundException  extends RuntimeException {
	PeopleNotFoundException(UUID uuid) {
		    super("Could not find uuid " + uuid);
		  }
}
