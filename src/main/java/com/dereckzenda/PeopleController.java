package com.dereckzenda;

import java.util.List;
import java.util.UUID;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
class PeopleController {
	
	private final PeopleRepository repository;

	  PeopleController(PeopleRepository repository) {
	    this.repository = repository;
	  }

	  // Aggregate root

	  @GetMapping("/people")
	  List<People> all() {
	    return repository.findAll();
	  }

	  @PostMapping("/people")
	  People newPeople(@RequestBody People newPeople) {
	    return repository.save(newPeople);
	  }

	  @GetMapping("/people/{uuid}")
	  People one(@PathVariable UUID uuid) {

	    return repository.findById(uuid).orElseThrow(() -> new PeopleNotFoundException(uuid));
	  }

	  @PutMapping("/people/{uuid}")
	  People replacePeople(@RequestBody People newPeople, @PathVariable UUID uuid) {

	    return repository.findById(uuid)
	      .map(people -> {
	    	  people.setSurvived(newPeople.getSurvived());
	    	  people.setPassengerClass(newPeople.getPassengerClass());
	    	  people.setName(newPeople.getName());
	    	  people.setSex(newPeople.getSex());
	    	  people.setAge(newPeople.getAge());
	    	  people.setSiblingsOrSpousesAboard(newPeople.getSiblingsOrSpousesAboard());
	    	  people.setParentsOrChildrenAboard(newPeople.getParentsOrChildrenAboard());
	    	  people.setFare(newPeople.getFare());
	          return repository.save(people);
	      })
	      .orElseGet(() -> {newPeople.setUUID(uuid);
	        return repository.save(newPeople);
	      });
	  }

	  @DeleteMapping("/people/{uuid}")
	  void deletePeople(@PathVariable UUID uuid) {
	    repository.deleteById(uuid);
	  }

}
